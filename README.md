# yaru

Yaru is the default theme for Ubuntu, backed by the community.

https://github.com/ubuntu/yaru

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/icons-and-themes/yaru/yaru.git
```

